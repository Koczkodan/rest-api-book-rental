from django.contrib import admin
from . models import Book,Author,Rental

admin.site.register(Book)
admin.site.register(Author)
admin.site.register(Rental)
# Register your models here.
