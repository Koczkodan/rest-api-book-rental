from .views import BookRudView

"""library_backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import include, path
from .views import (
    BookRudView,
    BookCreatreView,
    UserCreateAPIView,
    BooksView,
    UserView,
    UserDeleteView,
    AuthorsView,
    RentView,
    RentDestroyView)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('authors/', AuthorsView.as_view(), name='authors-get'),
    path('books/', BooksView.as_view(), name='books-get'),
    path('book/create/', BookCreatreView.as_view(), name='book-create'),
    path('book/<int:pk>/', BookRudView.as_view(), name='book-rd'),
    path('book/createRent/', RentView.as_view(), name='rent-create'),
    path('book/deleteRent/<int:pk>', RentDestroyView.as_view(), name='rent-create'),
    
    
    path('user/create/', UserCreateAPIView.as_view(), name='user-create'),
    path('users/', UserView.as_view(), name='users-get'),
    path('user/delete/<int:pk>', UserDeleteView.as_view(), name='users-delete'),
]