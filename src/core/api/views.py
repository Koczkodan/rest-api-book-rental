from rest_framework import generics
from core.models import Book,Author,Rental
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404 
from .serializers import (
    BooksSerializer,
    UserCreateSerializer,
    BookCreateSerializer,
    UserLoginSerializer,
    UserDetailsSerializer,
    UserDeleteSerializer,
    AuthorsSerializer,
    RentSerializer
) 
from django.db.models import Q
from rest_framework.views import APIView
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.authentication import TokenAuthentication
from rest_framework.status import HTTP_200_OK,HTTP_400_BAD_REQUEST,HTTP_204_NO_CONTENT


class AuthorsView(generics.ListAPIView):
    serializer_class = AuthorsSerializer
    queryset = Author.objects.all()

class BooksView(generics.ListAPIView):
    serializer_class = BooksSerializer
    def get_queryset(self):
        qs = Book.objects.all()
        query = self.request.GET.get('q')
        if query is not None:
            qs= qs.filter(
                Q(title__icontains=query)|
                Q(pub_date__icontains=query)|
                Q(authors__first_name__icontains=query)|
                Q(authors__last_name__icontains=query)).distinct()
        return qs

class BookCreatreView(generics.CreateAPIView):
    pass
    serializer_class = BookCreateSerializer
    authentication_classes=(TokenAuthentication,)

    def get_queryset(self):
        return Book.objects.all().prefetch_related('authors')


class UserView(generics.ListAPIView):
    serializer_class = UserDetailsSerializer
    def get_queryset(self):
        qs = User.objects.all()
        query = self.request.GET.get('q')
        if query is not None:
            qs= qs.filter(
                Q(username__icontains=query)).distinct()
        return qs

class UserDeleteView(generics.DestroyAPIView):
    lookup_field='pk'
    serializer_class = UserDeleteSerializer
    authentication_classes=(TokenAuthentication,)
    queryset = User.objects.all()



class BookRudView(generics.RetrieveDestroyAPIView):
    pass
    lookup_field='pk'
    serializer_class = BooksSerializer
    authentication_classes=(TokenAuthentication,)
    queryset = Book.objects.all()

class UserCreateAPIView(generics.CreateAPIView):
    pass
    lookup_field='pk'
    serializer_class = UserCreateSerializer
    authentication_classes=(TokenAuthentication,)
    def get_queryset(self):
        return Book.objects.all()


class RentView(generics.CreateAPIView):
    serializer_class = RentSerializer
    authentication_classes=(TokenAuthentication,)
    def get_queryset(self):
        return Rental.objects.all()

        

class RentDestroyView(generics.DestroyAPIView):
    lookup_field='pk'
    serializer_class = RentSerializer
    authentication_classes=(TokenAuthentication,)
    def get_queryset(self):
        return Rental.objects.all()


