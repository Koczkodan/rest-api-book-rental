from django.contrib.auth.models import User, Group
from rest_framework import serializers
from core.models import Book,Author,Rental
from django.db.models import Q


class AuthorsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Author
        fields = [
            'pk',
            'first_name',
            'last_name'
        ]

class RentalSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rental
        fields=['pk','personWhoRent']
        read_only_fields = ['pk']

class RentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rental
        fields=['pk','personWhoRent','rentedBook']
        read_only_fields = ['pk']

class BooksSerializer(serializers.ModelSerializer):
    authors = AuthorsSerializer(read_only=True,many=True)
    rental = RentalSerializer()
    class Meta:
        model = Book
        fields = [
            'pk',
            'title',
            'pub_date',
            'rental',
            'authors',
        ]

class BookCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Book
        fields = [
            'title',
            'pub_date' ,
            'authors',
        ]


class UserCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = [
            'username',
            'password',
        ]
        extra_kwargs={"password":
            {'write_only':True}
                        }
    def create(self,validated_data):
        username=validated_data['username']
        password=validated_data['password']

        user_obj=User(username=username)
        user_obj.set_password(password)
        user_obj.save()
        return validated_data

class UserDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            'pk',
            'username',
        ]  
class UserDeleteSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            'pk',
            'username',
        ]      

class UserLoginSerializer(serializers.ModelSerializer):
    token=serializers.CharField(allow_blank=True,read_only=True)
    username= serializers.CharField()
    class Meta:
        model = User
        fields = [
            'username',
            'password',
            'token'
        ]
        extra_kwargs={"password":
            {'write_only':True}
                        }

    def validate(self,data):
        user_obj=None
        password = data["password"]
        username= data.get('username',None)
        if not username:
            raise serializers.ValidationError("Username required")
        

        user = User.objects.filter(Q(username=username)).distinct()
        if user.exists() and user.count()==1:
            user_obj=user.first()
        else:
            raise serializers.ValidationError("This username does not exists")

        if user_obj:
            if not user_obj.check_password(password):
                raise serializers.ValidationError("Incorrect password")
        data["token"] = "saddsad"
        return data
            