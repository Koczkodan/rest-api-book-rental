from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Author(models.Model):
    first_name= models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    def __str__(self):
        return self.first_name +" "+ self.last_name

class Book(models.Model):
    title= models.CharField(max_length=30)
    pub_date = models.DateField()
    authors = models.ManyToManyField(Author)
    def __str__(self):
        return self.title

class Rental(models.Model):
    personWhoRent = models.ForeignKey(User, on_delete=models.CASCADE)
    rentedBook = models.OneToOneField(Book, on_delete=models.CASCADE)