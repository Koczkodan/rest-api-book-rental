# Generated by Django 3.0.6 on 2020-06-02 13:28

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Author',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=20)),
                ('last_name', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(choices=[('BE', 'Beletrystyka'), ('PU', 'Publicystyka'), ('PN', 'Popularno Naukowa'), ('DZ', 'Dziecięca'), ('POE', 'Poezja'), ('DRA', 'Dramat'), ('STR', 'Satyra'), ('EL', 'Pozostałe')], default='EL', max_length=3)),
            ],
        ),
        migrations.CreateModel(
            name='Book',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=30)),
                ('price', models.FloatField()),
                ('pub_date', models.DateField()),
                ('cover', models.CharField(max_length=30)),
                ('publishing_house', models.CharField(max_length=30)),
                ('isRented', models.BooleanField()),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Author')),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Category')),
            ],
        ),
    ]
