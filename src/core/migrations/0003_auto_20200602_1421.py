# Generated by Django 3.0.6 on 2020-06-02 14:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_auto_20200602_1331'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='book',
            name='author',
        ),
        migrations.RemoveField(
            model_name='book',
            name='cover',
        ),
        migrations.AddField(
            model_name='author',
            name='book',
            field=models.ManyToManyField(to='core.Book'),
        ),
    ]
