#!/bin/bash
echo "${0}: [1] Making migrations..."
python manage.py makemigrations
echo "${0}: [2] Applying migrations..."
python manage.py migrate
echo "${0}: [4] Running server..."
python manage.py runserver
