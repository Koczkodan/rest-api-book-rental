import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MainPageComponent} from './main-page/main-page.component'
import {BooksListComponent} from './books-list/books-list.component'
import {AddBookComponent} from './add-book/add-book.component'
import { AuthGuard } from './auth.guard';
import { UsersListComponent } from './users-list/users-list.component';
import { AddUserComponent } from './add-user/add-user.component';


const routes: Routes = [
{path: "", component: MainPageComponent},
{path: "books", component: BooksListComponent},
{path: "addbook", component: AddBookComponent, canActivate:[AuthGuard]},
{path: "users", component: UsersListComponent, canActivate:[AuthGuard]},
{path: "adduser", component: AddUserComponent, canActivate:[AuthGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents =[MainPageComponent,BooksListComponent,AddBookComponent,AddUserComponent,UsersListComponent]
