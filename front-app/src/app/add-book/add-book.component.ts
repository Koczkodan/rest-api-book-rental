import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.css'],
  providers:[ApiService]
})
export class AddBookComponent implements OnInit {
  authorsControl = new FormControl()
  selectedValue = ["1"]
  newBook
  authors
  info
  

  constructor(private api: ApiService,private router: Router) { 
    this.newBook = {authors:[],
      title:"",
      pub_date:null}
  }

  ngOnInit(): void {
    this.getAuthors()
  }
  getAuthors= ()=>{
    this.api.getAuthors().subscribe(
      data =>{
        this.authors = data;
      },
      error =>{
        console.log(error)
      }
    )


  }
  createBook():void{
    this.newBook.authors=this.selectedValue
    this.api.createBook(this.newBook).subscribe(
      data =>{
        this.info = data;
      },
      error =>{
        console.log(error)
      }
    )
    this.router.navigate(["books"])
  }

  getSelectedValue(author){
    console.log(author)
  }
  
}
