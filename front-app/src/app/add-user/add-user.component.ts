import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  password
  password2
  username
  valid

  constructor(private api: ApiService) { 
    this.password2=""
    this.password =""
    this.username = ""
  }

  ngOnInit(): void {
  }

  addUser(){
    if(this.validator()){
      let user = {username:this.username,password:this.password}

      this.api.createUser(user).subscribe(
        res => {},
        err=>{}
      )
    }
    else{
      alert("wrong validation")
    }

  }

  validator():boolean{
    let ans = true

    if(!!this.password==false || this.password == null || this.password!==this.password2){
      ans = false
    }
    if(!!this.username == false)
    ans= false

    return ans
  }


}
