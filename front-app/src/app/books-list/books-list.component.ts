import { Component, OnInit, Input } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-books-list',
  templateUrl: './books-list.component.html',
  styleUrls: ['./books-list.component.css'],
  providers: [ApiService]
})
export class BooksListComponent implements OnInit {
books
authors
searchField:string
info
isLogged
userID
  constructor(private api: ApiService) {
    this.isLogged = api.isLogged()
    this.userID = localStorage.getItem("id")
  } 


  getBooks = ()=>{
    this.api.getAllBooks(null).subscribe(
      data =>{
        this.books = data;
      },
      error =>{
        console.log(error)
      }
    )
  }

  findBooks= ()=>{
    this.api.getAllBooks(this.searchField).subscribe(
      data =>{
        this.books = data;
      },
      error =>{
        console.log(error)
      }
    )
  }

 


  ngOnInit(): void {
    console.log("aa")
    this.getBooks()
    
  }

  delete(book):void{
    let pk = book.pk
    this.api.deleteBook(pk).subscribe(
      data =>{
        this.info = data;
      },
      error =>{
        console.log(error)
      }
    )
    

  }

  rent(book):void{
    

    this.api.rentBook(book).subscribe(
      data =>{
        this.info = data;
      },
      error =>{
        console.log(error)
      }
    )

  }


  rentBack(book):void{

    this.api.rentBackBook(book).subscribe(
      data =>{
        this.info = data;
      },
      error =>{
        console.log(error)
      }
    )

  }

}
