import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  username
password
isLogged:boolean
isAdmin

constructor(private api: ApiService,private router: Router){
  this.isLogged = api.isLogged()
  this.isAdmin=api.isAdmin()
}

  ngOnInit(): void {
  }

logMeIn(){
  console.log({username:this.username,password:this.password})
  this.api.loginUser({username:this.username,password:this.password}).subscribe(
    response =>{
      localStorage.setItem("token",response.token)
      localStorage.setItem("id",response.id)
      window.location.reload()
      this.router.navigate(["books"])

    },
    error =>{
      console.log(error)
    }
  )
  
  
}

logout(){
  this.api.logOut()
}
  

}
