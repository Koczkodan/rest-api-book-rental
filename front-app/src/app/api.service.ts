import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  auth_token

  baseUrl = "http://127.0.0.1:8000/api"
  httpHeaders = new HttpHeaders({'Content-Type': 'application/json'})

  
  constructor(private http: HttpClient) { }

  getAllBooks(word): Observable<any>{
    let params = new HttpParams()
    if(word)
    params = params.append("q",word)
    return this.http.get(this.baseUrl + "/books/",
                        {headers: this.httpHeaders, params:params})
  }


  getAuthors(): Observable<any>{
    return this.http.get(this.baseUrl + "/authors/",
                        {headers: this.httpHeaders})
  }

  createBook(book): Observable<any>{
    this.auth_token = localStorage.getItem('token')
    var httpHeaders = new HttpHeaders({'Authorization': "token "+this.auth_token,'Content-Type': 'application/json'})
    return this.http.post(this.baseUrl + "/book/create/", book,
  {headers: httpHeaders})

  }
  deleteBook(pk): Observable<any>{
    this.auth_token = localStorage.getItem('token')
    var httpHeaders = new HttpHeaders({'Authorization': "token "+this.auth_token,'Content-Type': 'application/json'})
    return this.http.delete(this.baseUrl + "/book/"+pk+"/",
  {headers: httpHeaders})

  }

  rentBackBook(book): Observable<any>{
    this.auth_token = localStorage.getItem('token')
    var httpHeaders = new HttpHeaders({'Authorization': "token "+this.auth_token,'Content-Type': 'application/json'})
    let pk = book.rental.pk
    return this.http.delete(this.baseUrl + "/book/deleteRent/"+pk,
  {headers: httpHeaders})

  }

  rentBook(book){
    this.auth_token = localStorage.getItem('token')
    var userID = localStorage.getItem('id')
    let body = {personWhoRent:userID,rentedBook:book.pk}
    var httpHeaders = new HttpHeaders({'Authorization': "token "+this.auth_token,'Content-Type': 'application/json'})
    
    return this.http.post(this.baseUrl + "/book/createRent/", body,
  {headers: httpHeaders})
  }

  loginUser(body): Observable<any>{
    
    return this.http.post("http://127.0.0.1:8000/auth/",body,
  {headers: this.httpHeaders})
  }


  createUser(user): Observable<any>{
    this.auth_token = localStorage.getItem('token')
    var httpHeaders = new HttpHeaders({'Authorization': "token "+this.auth_token,'Content-Type': 'application/json'})
  
    return this.http.post(this.baseUrl+"/user/create/",user,
  {headers: httpHeaders})
  }

  deleteUser(user): Observable<any>{
    this.auth_token = localStorage.getItem('token')
    var httpHeaders = new HttpHeaders({'Authorization': "token "+this.auth_token,'Content-Type': 'application/json'})
    let pk = user.pk
    return this.http.delete(this.baseUrl + "/user/delete/"+pk,
  {headers: httpHeaders})
  }

  getUsers(word): Observable<any>{
    let params = new HttpParams()
    if(word)
    params = params.append("q",word)
    return this.http.get(this.baseUrl + "/users/",
                        {headers: this.httpHeaders, params:params})
  }





  isLogged(){
    return !!localStorage.getItem('token')
  }

  logOut(){
    localStorage.clear()
  }

  isAdmin():boolean{
    let tk = localStorage.getItem('token')
    if(tk == 'f6c49975ff0075562d9127dd68976b3a955a5736')
    return true
    else return false
  }

}
