import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {

  users
  searchField
  

  constructor(private api: ApiService) {
    this.getUsers()

   }

  ngOnInit(): void {
  }

  getUsers(){
    this.api.getUsers(this.searchField).subscribe(
      res =>{this.users = res
      console.log(res)},
      err => {}
    )
  }

  delete(user){
    this.api.deleteUser(user).subscribe(
      res => {},
      err =>{}
    )
  }

}
